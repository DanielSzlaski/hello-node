
'use strict';
const Api = require('kubernetes-client');
const core = new Api.Core({
  url: 'http://localhost:8000',
  version: 'v1',  // Defaults to 'v1'
  namespace: 'default' // Defaults to 'default'
});
/*
function print(err, result) {
  console.log(result);
  console.log(JSON.stringify(err || result, null, 2));
}
*/

//const core = new Api.Core(Api.config.getInCluster());


const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App

const app = express();
app.get('/', (req, res) => {
  res.send('Response from Service ;) \n');
  console.log('Response from Service ;)');
});

app.get('/hello-node', (req, res) => {
  res.send('Hello to U 2 ! \n');
  console.log('Response from Service ;)');
});


app.listen(PORT, HOST);


