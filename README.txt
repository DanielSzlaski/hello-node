# Installataion:
## Docker
docker build -t hello-node .
docker run --rm -p 8080:8080 hello-node

docker tag hello-node danielszlaski/hello-node
docker push danielszlaski/hello-node

## K8s
kubectl apply -f k8s

kubectl port-forward hello-node-6768c76474-gv6rl 8080
